#include "matrix_operation.h"

void inv_mat(double mat_in[SIZE][SIZE], double mat_out[SIZE][SIZE]){

	//double a[4][4] = { {1,2,0,-1},{-1,1,2,0},{2,0,1,1},{1,-2,-1,1} }; //入力用の配列
	//double inv_a[4][4]; //ここに逆行列が入る
	double buf; //一時的なデータを蓄える

	double temp[SIZE][SIZE];

	for (int i = 0; i < SIZE; i++) {
		for (int j = 0;j < SIZE; j++) {
			temp[i][j] = mat_in[i][j];
		}
	}

	//単位行列を作る
	for (int i = 0; i < SIZE; i++) {
		for (int j = 0; j < SIZE; j++) {
			mat_out[i][j] = (i == j) ? 1.0 : 0.0;
		}
	}
	//掃き出し法
	for (int i = 0; i < SIZE; i++) {
		buf = 1 / mat_in[i][i];
		for (int j = 0; j < SIZE; j++) {
			mat_in[i][j] *= buf;
			mat_out[i][j] *= buf;
		}
		int j;
		for (j = 0; j < SIZE; j++) {
			if (i != j) {
				buf = mat_in[j][i];
				for (int k = 0; k < SIZE; k++) {
					mat_in[j][k] -= mat_in[i][k] * buf;
					mat_out[j][k] -= mat_out[i][k] * buf;
				}
			}
		}
	}

	for (int i = 0; i < SIZE; i++) {
		for (int j = 0; j < SIZE; j++) {
			mat_in[i][j] = temp[i][j];
		}
	}

	//逆行列を出力
	//for (int i = 0; i < n; i++) {
	//	for (int j = 0; j < n; j++) {
	//		printf(" %f", inv_a[i][j]);
	//	}
	//	printf("\n");
	//}
}