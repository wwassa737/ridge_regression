# description 
学習データのリッジ回帰をおこない、
検証データのR2スコアを出力するプログラム

# interface
* 入力(ローカル変数)
	* 学習データ
		```double x[],y[]``` 

	* 検証データ
		```double x_val[],y_val[]``` 

* 出力
  * 各項の係数
  * trainデータのR2スコア
  * valデータのR2スコア

# parameter
```project.h```で設定を行う

```Cpp
#define BASIS_NUM 4		/* 基底数 */```
#define DATA_SIZE 61	/* 学習データ数 */
#define ALPHA 0.1		/*　正規化係数　*/
```

* 基底数 
(基底数-1)次の多項式モデルで学習する
$$y=a_0+a_1 x+a_2 x^2+...+a_N x^N $$

* 学習データ数
学習データ、検証データ長をここに与える

* 正規化係数
学習データに過学習することを防ぐための係数

# Usage
```Cpp
/* インスタンス */
Ridge ridge;

/* 初期化 */ 
ridge.init();

/* 学習 */
ridge.fit(x, y);

/* validation */
ridge.calc_validation_score(x, y, x_val, y_val);
```