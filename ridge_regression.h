#include "project.h"

class Ridge 
{

private:
		double alpha;
		double coeff[BASIS_NUM];
		double y_pref[DATA_SIZE];
		/* R2係数 */
		double score;	
		/* R2係数(validation) */
		double score_validation;
		
public:
		/* コンストラクタ */
		Ridge(void);

		void init(void);

		void fit(double[DATA_SIZE], double[DATA_SIZE]);
		/* 
		x:説明変数
		y:目的変数
		y_pref:予測値
		*/
		void calc_validation_score(double[DATA_SIZE], double[DATA_SIZE], double[DATA_SIZE], double[DATA_SIZE]);

};