#include "project.h"

#ifndef BASIS_NUM
#define SIZE 4
#endif

#ifdef BASIS_NUM
#define SIZE BASIS_NUM
#endif


void inv_mat(double mat_in[SIZE][SIZE], double mat_out[SIZE][SIZE]);