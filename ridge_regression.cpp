#include "project.h"
#include "ridge_regression.h"
#include "matrix_operation.h"
#include <cstdio>

Ridge::Ridge() {
	/* コンストラクタ */
	this->alpha = ALPHA;

}

void Ridge::init() {
	for (int i = 0; i < BASIS_NUM; i++) {
		this->coeff[i] = 0.0;
	}
}

void Ridge::fit(double x[DATA_SIZE], double y[DATA_SIZE])
{
	/*
x:説明変数
y:目的変数
y_pref:予測値
*/

	double temp[DATA_SIZE];
	double x_mat[DATA_SIZE][BASIS_NUM];
	double temp2[BASIS_NUM][BASIS_NUM];
	double temp3[BASIS_NUM][DATA_SIZE];
	double temp_inv[BASIS_NUM][BASIS_NUM];
	/* Xの作成 */
	for (int i = 0; i < DATA_SIZE; i++) {
		temp[i] = 1.0f;
		x_mat[i][0] = temp[i];
	}

	for (int i = 0; i < DATA_SIZE; i++) {
		for (int j = 0; j < BASIS_NUM - 1; j++) {
			temp[i] *= x[i];
			x_mat[i][j + 1] = temp[i];
		}
	}

	/* (X.T X)^(-1) X.T y */

	/* X.T X*/


	double sum = 0.0;
	for (int j = 0; j < BASIS_NUM; j++) {
		for (int i = 0; i < BASIS_NUM; i++) {


			/* Xki Xkj */
			sum = 0.0;
			for (int k = 0; k < DATA_SIZE; k++) {
				sum += x_mat[k][i] * x_mat[k][j];
			}

			if (i == j) {
				sum += this->alpha;/* 正規化 */
			}

			temp2[i][j] = sum;

		}
	}

	//for (int j = 0; j < BASIS_NUM; j++) {
	//	for (int i = 0; i < BASIS_NUM; i++) {

	//		printf("%f\t", temp2[i][j]);

	//	}
	//	printf("\n");

	//}



	/* (X.T X)^(-1) */
	inv_mat(temp2, temp_inv);

	//逆行列を出力
	//printf("\n");
	//for (int i = 0; i < BASIS_NUM; i++) {
	//	for (int j = 0; j < BASIS_NUM; j++) {
	//		printf("%f\t", temp_inv[i][j]);
	//	}
	//	printf("\n");
	//}

	/* (X.T X)^(-1) X */
	// Hik Xkj.T [4*4][4*61]

	sum = 0.0;
	for (int j = 0; j < DATA_SIZE; j++) {
		for (int i = 0; i < BASIS_NUM; i++) {

			/* ik Xjk */
			sum = 0.0;
			for (int k = 0; k < BASIS_NUM; k++) {
				sum += temp_inv[i][k] * x_mat[j][k];
			}
			temp3[i][j] = sum;

		}
	}

	/* (X.T X)^(-1) X t*/

	for (int i = 0; i < BASIS_NUM; i++) {

		sum = 0;
		for (int k = 0; k < DATA_SIZE; k++) {
			sum += temp3[i][k] * y[k];
		}
		coeff[i] = sum;
	}

	/* 結果表示 */
	std::printf("\n");
	for (int i = 0; i < BASIS_NUM; i++) {
		std::printf("%f\t", coeff[i]);
	}
	std::printf("\n");


	sum = 0;

	/* 回帰 */
	for (int i = 0; i < DATA_SIZE; i++) {
		sum = 0;
		for (int k = 0; k < BASIS_NUM; k++) {

			sum += x_mat[i][k] * coeff[k];

		}
		y_pref[i] = sum;
	}

	for (int i = 0; i < BASIS_NUM; i++) {

		this->coeff[i] = coeff[i];
	}

	for (int i = 0; i < DATA_SIZE; i++) {

		this->y_pref[i] = y_pref[i];
	}

}

void Ridge::calc_validation_score(double x[DATA_SIZE], double y[DATA_SIZE],double x_val[DATA_SIZE], double y_val[DATA_SIZE]) {
	/* Validation */
	double r2;

	r2 = 0;
	double sum1 = 0;
	double sum2 = 0;
	double sum = 0;

	/* 平均 */
	for (int i = 0; i < DATA_SIZE; i++) {
		sum += y[i];
	}
	double y_ave = sum / DATA_SIZE;

	/* Evaluation */
	for (int i = 0; i < DATA_SIZE; i++) {
		sum += (y_ave - y[i]) * (y_ave - y[i]);
		sum1 += (this->y_pref[i] - y[i]) * (this->y_pref[i] - y[i]);
	}

	this->score = 1.0 - sum1 / sum;

	std::printf("R2(train):\t%f\n", this->score);

	sum = 0, sum1 = 0, sum2 = 0;
	/* Validation */
	for (int i = 0; i < DATA_SIZE; i++) {
		sum += (y_ave - y_val[i]) * (y_ave - y_val[i]);
		sum1 += (this->y_pref[i] - y_val[i]) * (this->y_pref[i] - y_val[i]);
	}
	this->score_validation = 1.0 - sum1 / sum;
	std::printf("R2(val):\t%f\n", this->score_validation);

	FILE *fp;

	fp = std::fopen("test.txt", "w");
	if (fp == NULL) {

		//失敗と表示し終了

		std::printf("ファイルオープン失敗\n");

	}

	std::fprintf(fp, "==================\n");
	for (int i = 0; i < BASIS_NUM; i++) {
		std::fprintf(fp, "%f\t", this->coeff[i]);
	}
	std::fprintf(fp, "==================\n");
	std::fprintf(fp, "y_pref\n");

	for (int i = 0; i < DATA_SIZE; i++) {
		std::fprintf(fp, "%f\n", this->y_pref[i]);
	}


	//ファイルを閉じる

	std::fclose(fp);


}